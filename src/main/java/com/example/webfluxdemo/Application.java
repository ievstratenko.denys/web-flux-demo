package com.example.webfluxdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.UnicastProcessor;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public HandlerMapping webSocketMapping(WebSocketHandler webSocketHandler) {
		Map<String, Object> map = new HashMap<>();
		map.put("/game", webSocketHandler);
		SimpleUrlHandlerMapping simpleUrlHandlerMapping = new SimpleUrlHandlerMapping();
		simpleUrlHandlerMapping.setUrlMap(map);
		simpleUrlHandlerMapping.setOrder(0);
		return simpleUrlHandlerMapping;
	}

	@Bean
	public WebSocketHandlerAdapter handlerAdapter() {
		return new WebSocketHandlerAdapter();
	}

	@Bean
	public WebSocketHandler webSocketHandler(CheckService checkService) {
		return session -> {
			UnicastProcessor<String> processor = UnicastProcessor.create();
			Flux<String> messages = processor.replay(0).autoConnect();
			session.receive()
					.map(WebSocketMessage::getPayloadAsText)
					.subscribe(m -> checkService.check(m).forEach(processor::onNext));
			return session.send(messages.map(session::textMessage));
		};
	}
}