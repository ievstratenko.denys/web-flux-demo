package com.example.webfluxdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
class CheckService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CheckService.class);

	@Autowired
	private RandomNumberRunnable randomNumberRunnable;

	@PostConstruct
	void init() {
		new Thread(randomNumberRunnable).start();
	}

	List<String> check(String message) {
		LOGGER.info("Processing message: {}", message);
		try {
			int number = Integer.parseInt(message.trim());
			if (number < randomNumberRunnable.getMinValue() || number > randomNumberRunnable.getMaxValue()) {
				return List.of("ERROR: number must be between " + randomNumberRunnable.getMinValue() + " and " + randomNumberRunnable.getMaxValue());
			} else {
				String resolution = isWin(number) ? "You win" : "You lose";
				return List.of(resolution, "Start round");
			}
		} catch (NumberFormatException e) {
			return List.of("ERROR: not a number");
		} catch (InterruptedException e) {
			e.printStackTrace();
			return List.of("ERROR: " + e.getMessage());
		}
	}

	private boolean isWin(int number) throws InterruptedException {
		LOGGER.info("Registering a new try: {}", number);
		Try t = new Try(number);
		randomNumberRunnable.addTry(t);
		synchronized (t) {
			t.wait();
		}
		return t.isWin();
	}
}