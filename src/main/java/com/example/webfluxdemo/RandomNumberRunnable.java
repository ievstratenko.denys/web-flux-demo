package com.example.webfluxdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RandomNumberRunnable implements Runnable {

	private static final Logger LOGGER = LoggerFactory.getLogger(RandomNumberRunnable.class);

	@Value("${minValue}")
	private int minValue;

	@Value("${maxValue}")
	private int maxValue;

	@Value("${roundDurationMillis}")
	private int roundDurationMillis;

	@Value("${rounds}")
	private int rounds;

	private final List<Try> tries = new ArrayList<>();

	void addTry(Try t) {
		synchronized (tries) {
			tries.add(t);
		}
	}

	int getMinValue() {
		return minValue;
	}

	int getMaxValue() {
		return maxValue;
	}

	@Override
	public void run() {
		for (int i = 0; i < rounds; i++) {
			int randomNumber = (int) ((maxValue - minValue + 1) * Math.random()) + minValue;
			LOGGER.info("Starting a new round with a random number={}", randomNumber);
			try {
				Thread.sleep(roundDurationMillis);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			} finally {
				synchronized (tries) {
					tries.forEach(t -> {
						t.setWin(t.getNumber() == randomNumber);
						LOGGER.info("Got an answer for {}. Win={}", t.getNumber(), t.isWin());
						synchronized (t) {
							t.notifyAll();
						}
					});
					tries.clear();
				}
			}
		}
	}
}