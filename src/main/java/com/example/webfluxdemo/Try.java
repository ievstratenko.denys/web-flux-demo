package com.example.webfluxdemo;

class Try {

	private int number;
	private boolean win;

	Try(int number) {
		this.number = number;
	}

	int getNumber() {
		return number;
	}

	void setWin(boolean win) {
		this.win = win;
	}

	boolean isWin() {
		return win;
	}
}
