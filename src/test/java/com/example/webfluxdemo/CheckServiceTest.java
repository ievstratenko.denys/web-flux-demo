package com.example.webfluxdemo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CheckServiceTest {

	@Autowired
	private RandomNumberRunnable randomNumberRunnable;

	@Autowired
	private CheckService checkService;

	@Test
	public void testValid() throws ExecutionException, InterruptedException {
		ForkJoinPool forkJoinPool = new ForkJoinPool(randomNumberRunnable.getMaxValue() - randomNumberRunnable.getMinValue() + 1);
		List<String> responses = forkJoinPool.submit(() ->
				IntStream.range(randomNumberRunnable.getMinValue(), randomNumberRunnable.getMaxValue() + 1)
						.mapToObj(Integer::toString)
						.parallel()
						.map(checkService::check)
						.flatMap(List::stream)
						.collect(toList())
		).get();

		long winCount = responses.stream().filter(r -> r.equals("You win")).count();
		Assert.assertEquals(1, winCount);

		long loseCount = responses.stream().filter(r -> r.equals("You lose")).count();
		Assert.assertEquals(randomNumberRunnable.getMaxValue() - randomNumberRunnable.getMinValue(), loseCount);
	}

	@Test
	public void testInvalid() {
		String expectedRangeError = "ERROR: number must be between " + randomNumberRunnable.getMinValue() + " and " +
				randomNumberRunnable.getMaxValue();
		Assert.assertEquals(expectedRangeError, checkService.check("0").get(0));
		Assert.assertEquals(expectedRangeError, checkService.check("11").get(0));

		String expectedNumberFormatError = "ERROR: not a number";
		Assert.assertEquals(expectedNumberFormatError, checkService.check("abc").get(0));
	}

}
