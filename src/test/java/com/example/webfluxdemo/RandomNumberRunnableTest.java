package com.example.webfluxdemo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RandomNumberRunnableTest {

	@Autowired
	private RandomNumberRunnable randomNumberRunnable;

	@Test
	public void testOneWin() {
		List<Try> tries = IntStream.range(randomNumberRunnable.getMinValue(), randomNumberRunnable.getMaxValue() + 1)
				.mapToObj(Try::new)
				.collect(Collectors.toList());

		tries.forEach(randomNumberRunnable::addTry);
		randomNumberRunnable.run();

		long winCount = tries.stream().filter(Try::isWin).count();

		Assert.assertEquals(1, winCount);
	}

}
